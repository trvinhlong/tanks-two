// server/app.js
const express = require('express');
const app = express();
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const axios = require('axios');
const config = require('./config');

const photos = require('./routes/photos');
const facebook = require('./routes/facebook');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

app.use(passport.initialize());
app.use(passport.session());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/photos', photos);
app.use('/facebook', facebook);
app.use('/images', express.static(path.join(__dirname, 'public/images')));
app.use('/webhook', (req, res) => {
  axios.post('https://slack.com/api/chat.postMessage', {
    "channel": "@long_tv",
    "username": "GHTK",
    "text": JSON.stringify(req.body)}, {
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + config.slackToken
    }
  })

  const csvWriter = createCsvWriter({
    path: require('path').resolve(__dirname, '../ghtk.csv'),
    header: [
      { id: 'date', title: 'DATE' },
      { id: 'req', title: 'REQUEST' }
    ],
    append: true
  });
  const records = [
    { date: new Date(Date.now()).toLocaleString(), req: JSON.stringify(req.body) }
  ];

  csvWriter.writeRecords(records)       // returns a promise
    .then(() => {
      res.send('OK')
    })
    .catch(() => {
      res.status(500).send('Something went wrong')
    })
});

// Setup logger
app.use(logger(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', 'dist')));


// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'dist', 'index.html'));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
